import { IsNotEmpty, MinLength, Min } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @Min(0)
  price: number;
}
